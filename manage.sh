#!/bin/bash
# This is a silent bash script, yay.
# Copyright (C) Alexander Nicholson and Ryan Veazey.

# manage.sh - Enable admin to manage their environment and view users etc.

# WARNING: POINT OF TRUST. THERE IS NO CHECKING FROM THESE BASH SCRIPTS FOR LEGITIMATE USERS. ALL TRUST IS PUT IN CORRECT AUTH WITHIN THE FRONT-END.

# The database works in the following way:
	# QUERY: Create user \
	#		Plain text info inserted into ($username.txt) \
	# 									Plain text username entered into (allusers.txt)

	# QUERY: Info on user \
	#		Check ((allusers.txt) for existence of user \
	#									If it exists:
	#											Read the relevant ($username.txt)
	#									Else:
	#											Error

# All plain-text database files must be stored *outside* of the web directory.

# The "user_skeleton" folder *MUST* be placed into the relevant user skeleton folder before this script will work.
#----------------------------------------------------------------------------------------------------------------------------------------------------

# Begin Variables

abuse_folder=""
username=$1
password=$2
hostname=$3
plan_name=$4
billing_period=$5
logfile="flamingword.log"

# Grab the script variables
# echo $1 $2 $3 $4 $5 ' -> echo $1 $2 $3 $4 $5'

# $1 is username
# $2 is password
# $3 is hostname
# $4 is plan_name
# $5 is billing period

#----------------------------------------------------------------------------------------------------------------------------------------------------

function make_env {
	# Create the environment for the user:
	# Create a user called $username (checking for, and handling, collisions of names)
	# Unzip skeleton.zip into directory called /home/$username/public_html/ (which is created by the user skeleton)
	# Create database user called $username
	# Create database password called $username
	# Enter the database details into Wordpress's config file
	# Return whether successful or not
	
}

function remove_env {
	# Remove the user's environment:
	# Remove directory called /home/$username/public_html/
	
	# NOTE: THIS IS NOT TO BE CALLED EXCEPT FROM remove_user and refresh_env
	
	if [[ ! -d /home/$username/public_html ]];
	then
	    echo "/home/$username/public_html/ does not exist! Exiting..." | tee -a $logfile
	    exit 1
	else
	    echo "Removing the users data..." | tee -a $logfile
	    rm -rf /home/$username/public_html/
	fi
	
}

function suspend_env {
	# Suspend the user's environment:
	# Zip recursively all files/folders in /home/$username/public_html/
	# Copy zip to $abuse_folder
	# Remove all files and folders in (but not the folder itself) /home/$username/public_html/
	# Create a file called "index.html" with a basic "<h1>This user has been suspended. Please contact the administration.</h1>"
	# Return whether successful or not
	
	if [[ -d /home/$username/public_html ]];
	then
	    zip -r $username.zip /home/$username/public_html
	    mv $abusefolder
	    rm -rf /home/$username/public_html/*
	    touch index.html
	    cat "<h1>This user has been suspended. Please contact support" >> index.html
	else
	    echo "Exiting - $username does not exist!" | tee -a $logfile
	    exit 1
	fi

}

function refresh_env {
	# Refresh the user's environment:
	# This will not remove any database elements, only broken themes/plugins/settings etc. will be fixed by this.
	
	# Copy WordPress database information into variables
	# Remove all files and folders in (but not the folder itself) /home/$username/public_html/
	# Unzip skeleton.zip into directory called /home/$username/public_html/ (which is created by the user skeleton)
	# Enter the database details into Wordpress's config file
	# Return whether successful or not
}

function make_user {
	# Make the user
	# QUERY: Make user \
	#		Plain text info inserted into ($username.txt) \
	# 									Plain text username entered into (allusers.txt)
}

function remove_user {
	# Remove the user
	# QUERY: Remove user \
	#		Remove ($username.txt) \
	# 									Plain text username removed from (allusers.txt)
}

function info_user {
	# Get information about a user
	# QUERY: Info on user \
	#		Check ((allusers.txt) for existence of user \
	#									If it exists:
	#											Read the relevant ($username.txt)
	#									Else:
	#											Error
}

function bill_user {
	# Bill the user after a set time has passed
}

function paid_user {
	# Set the new time for billing
}
